# Xprize BiodivX bioinformatics

Welcome to this repository that is aimed at compiling all the information required to run the eDNA pipeline available [here](https://gitlab.ethz.ch/tsanche/edna-minion) during the Xprize competition. 

## How to create or extend the reference database?
The current reference database is provided in the `/home/biodivx/edna-minion/data/reference_database/ref_db_xprize/` folder of each laptop. This database is a concatenation of BOLD and MIDORI2 for the CO1, rbcl and cytb primers (the code used to create this database is in the `extra` folder of the `edna-minon` repository). Entries with the same sequence have been collapsed to the one with a BIN available or the first occurrence. Taxonomy has been harmonized with GBIF (**this needs to be check again**). Finally, the fasta file has been indexed for blast using the command `makeblastdb -in ref_db_xprize.fasta -dbtype nucl`.

If you wish to add new sequences to this database, you first need to produce a .fasta file with the sequences and their description following this format to be compatible with the current database:
`>ref_db_id;marker_code;database_name;BOLD_bin;kingdom;phylum;class;order;family;genus;species`

and use the value `None` when the information is missing:

`>HIDNA022-14;rbcLa;BOLD_Public.21-Jun-2024;None;Plantae;Magnoliophyta;Magnoliopsida;Rosales;Rosaceae;Fragaria;Fragaria x ananassa`

Then concatenate both fasta files (`cat ref_db_xprize.fasta new_seq.fasta > extended_ref_db.fasta`) and run `makeblastdb -in extended_ref_db.fasta -dbtype nucl` in the same folder.
Alternatively, you can keep both databases separated and run the pipeline twice by creating two configuration files, one with the parameter `db = "path/to/ref_db_xprize.fasta"` and the other with `db = "path/to/new_seq.fasta"`. For this you would need to run `makeblastdb` on each fasta file separately.

## Inputs
The pipeline needs 3 inputs:
- the configuration file, you can find a detailed example in the `edna-minion` repository in the `config` folder. A config file for the xprize is included in this repository (`xprize_pipeline_config.toml`) and missing piece of information are commented for you to complete
- the list of the sample tags used for demultiplexing in a .csv file (only needed because we are using custom tags). A file is already included in this repository (`xprize_sample_tags.csv`) are `barcode_name` (which could be the sample name), `TAG_F`, `TAG_R`, `F_PRIMER_SEQ` and `R_PRIMER_SEQ`
- a folder with .pod5 files or a basecalled bam files (you will use MinKNOW for basecalling so in the xprize context you will provide a path to .bam files in the config file as explained in the workflow section).

## Outputs
The main output is a table that contains all the information of the taxonomic assignment. You can find an example in this repository `xprize_rbcl_cytb_custom_2024-07-03_15_28_33_results.tsv`. A pie-chart along alternative formats are also produced. If you wish to generate plots of the phred quality and length of the reads, you can use the functions in the `edna-minion` repository in the `edna_minion_workflow/visualization.py` file (you would need to run these function inside the docker container to have all the dependencies). 

## The workflow
1. Use the MinKNOW interface to start the sequencing and the live basecalling. Set up the parameters and make sure that:
    - you added the laptop nickname (*waterfall*, *river* or *forest*) in the run name to make the name unique across all laptops (this should prevent difficulties when merging all the results)
    - a quality threshold is set up (qscore>12)
    - you are only trimming the adapters and not the rest otherwise you will be not able to demultiplex
    - a minimum length is set up
    - you selected the *sup* model for the basecalling
    - you checked the option to output **.bam** files

2. Stop the sequencing and select the option to continue the basecalling until the end (if you are running out of time you can also stop the basecalling, .bam files are written every x reads depending on what you have set up previously)

3. Copy the folder with the .bam files to the user home directory with `cp -r /var/lib/minknow/data/run_name/path/to/pod5_pass /home/biodivx`

4. Check the pipeline configuration file `/home/biodivx/xprize-biodivx-bioinformatics/xprize_pipeline_config.toml` and make sure that the path to the .bam files is set up. An example of pipeline configuration file with comments is available [here](https://gitlab.ethz.ch/tsanche/edna-minion/-/blob/main/config/workflow_example_config.toml)

5. Start the docker container with the following command line `docker run -it --rm --gpus all -v $HOME:$HOME -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) thesanc/edna_minion_tools bash` (you can optionally start the container in a `screen` command if you want)

6. Move to the edna-minion folder with `cd /home/biodivx/edna-minion/`

7. Start the pipeline with `python3 workflow.py -c /home/biodivx/xprize-biodivx-bioinformatics/xprize_config.toml`

8. Check the table in `/home/biodivx/edna-minion/output/run_name/results` with all the results. If the `max_target_seqs` in the pipeline config file is set to a value greater that 1, please review the multiple hits and modify the column `keep_for_analysis` accordingly. Only one hit should be chosen per query.

9. Enjoy!

## How to resume a run?

If you had to stop the pipeline for any reason, you can resume it before the failing step. To do so, first modify the config file in the output folder to skip the steps already done and change the desired parameters. Then you can run the pipeline with the `-r` flag like this: `python3 workflow.py -c config/output/run_xyz/run_xyz_config.toml -r`. 

## Tips
- You might see some errors and weird temporary files created during the clustering phase. This is due to `amplicon_sorter` not handling well empty files and files with few reads. You can ignore them for the inputs that are expected to be empty.
- The clustering with `amplicon_sorter` is also a step that can take some times so don't panic if the pipeline seems to be stuck during this step. You can check if the process is doing something by looking at the `htop` command. If you think it is taking too much time, you can restart the run after modifying the clustering parameters in the pipeline config file (see `amplicon_sorter` documentation).
- When inside the container, only write files in the `/home` directory as it is the directory mounted (flag `-v $HOME:$HOME` of `docker run`). Files written in other path will disapear once the container is stopped.
- You can install whatever you need in the docker container but depending where the package is installed, it might disappear after the container is stopped.
- If you are not sure of what you are doing and you cannot find the information here, call me :)

## Output format
This repository includes an example of output file produced by the edna-minion-pipeline [here](https://gitlab.ethz.ch/tsanche/xprize-biodivx-bioinformatics/-/blob/main/xprize_rbcl_cytb_custom_2024-06-27_20_29_33_results.tsv). The columns are described in the following table:

| Column name                 | Description                                                                                                                                                    |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| qseqid                      | Determined by `outfmt` in the pipeline config file: ID of the query sequence (it is the MOTU id)                                                               |
| sseqid                      | Determined by `outfmt` in the pipeline config file: Hit ID in the reference database (here it is the full description)                                         |
| pident                      | Determined by `outfmt` in the pipeline config file: Percentage of identity                                                                                     |
| length                      | Determined by `outfmt` in the pipeline config file: Length of the matching segments                                                                            |
| mismatch                    | Determined by `outfmt` in the pipeline config file: Length of the mismatching segments                                                                         |
| gapopen                     | Determined by `outfmt` in the pipeline config file: Number of gaps                                                                                             |
| qstart                      | Determined by `outfmt` in the pipeline config file: Where the match start in the query                                                                         |
| qend                        | Determined by `outfmt` in the pipeline config file: Where the match end in the query                                                                           |
| sstart                      | Determined by `outfmt` in the pipeline config file: Where the match start in the sequence from the reference database                                          |
| send                        | Determined by `outfmt` in the pipeline config file: Where the match end in the sequence from the reference database                                            |
| evalue                      | Determined by `outfmt` in the pipeline config file: Number of expected hits of similar quality that could be found just by chance                              |
| bitscore                    | Determined by `outfmt` in the pipeline config file: size of a sequence database in which the current match could be found just by chance (log2 and normalized) |
| qseq                        | Determined by `outfmt` in the pipeline config file: Part of the query sequence aligned to the sequence in the reference database                               |
| blast_rank                  | Depends on the parameter `max_target_seqs` in the pipeline config file. Gives the rank of the hit when `blastn` outputs more than one hit                      |
| run_name                    | Name of the run defined in the pipeline config file                                                                                                            |
| sample_barcode              | ID of the sample where the reads come from                                                                                                                     |
| reference_database_sequence | Sequence from the reference database                                                                                                                           |
| motu_sequence               | Consensus sequence from the clustering step                                                                                                                    |
| ref_db_id                   | Setup with `reference_database.format` in the pipeline config file: ID of the sequence in the reference database                                               |
| marker_code                 | Setup with `reference_database.format` in the pipeline config file: Name of the marker in the reference database (e.g., COI, rbcl etc...)                      |
| database_name               | Setup with `reference_database.format` in the pipeline config file: Source of the sequence in the reference database                                           |
| BOLD_bin                    | Setup with `reference_database.format` in the pipeline config file: BOLD's BIN when available                                                                  |
| kingdom                     | Phylogeny                                                                                                                                                      |
| phylum                      | Phylogeny                                                                                                                                                      |
| class                       | Phylogeny                                                                                                                                                      |
| order                       | Phylogeny                                                                                                                                                      |
| family                      | Phylogeny                                                                                                                                                      |
| genus                       | Phylogeny                                                                                                                                                      |
| species                     | Phylogeny                                                                                                                                                      |
| nb_reads                    | Number of reads in the cluster                                                                                                                                 |
| keep_for_analysis           | Select the hit to keep for the downstream analysis (by default True when `blast_rank` is 1, only one hit should be True peer query)                            |

## Hardware setup
Three laptops (nicknamed "Waterfall", "River" and "Forest") have been set up. All three have GPUs, Docker, offline MinKNOW, the pipeline and its associated docker image preinstalled. 

## Software setup
The steps described in this section have already been performed on the three team's laptops, therefore you do not need to install these software again. On each laptop, the pipeline has already been downloaded in the home folder (`/home/biodivx/edna-minion`). This tutorial is available in the `/home/biodivx/xprize-biodivx-bioinformatics` folder. In case you want to perform the basecalling using the docker container instead of MinKNOW, all the models are available in `/home/biodivx/dorado_models`. You can follow these steps if you wish to setup a new computer. 

### eDNA minion pipeline
This pipeline along a docker image that contains multiple softwares useful for eDNA MinION analysis is available [here](https://gitlab.ethz.ch/tsanche/edna-minion). To download it, please use the following command line `git clone https://gitlab.ethz.ch/tsanche/edna-minion.git`. After downloading it, you can update it by using going in the repository folder with `cd edna-minion` and use `git pull`. Similarly, you can use the command `docker pull thesanc/edna_minion_tools:latest` to retrieve the latest version of the docker image.

### Docker
Docker can be installed by following the intruction [here](https://docs.docker.com/engine/install/ubuntu/) and the NVIDIA Container Toolkit (to unlock GPU for docker containers) by following the intruction [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html).
Then the current user should be added to the docker group by using the command line `sudo usermod -aG docker $USER`.

### Installing **offline** MinKNOW on Linux

To install offline MinKNOW for Linux, please uninstall the current MinKNOW you have in your computer and install the latest version from this link:

https://community.nanoporetech.com/downloads

Afterwards, please follow these instructions:
1. Please remove the previous attempt to disable pings by running `sudo rm /opt/ont/minknow/conf/installation_overrides.toml`
2. Disable the WiFi to prevent connection after restarting  
3. Shutdown the computer/device  
4. Remove the ethernet cable  
5. Power on the computer/device  
6. Open a terminal and run the following command: 
`echo "on_acquisition_ping_failure = 'ignore'" | sudo tee /opt/ont/minknow/conf/installation_overrides.toml`
7. Restart the MinKNOW service by running the following commands: 
```bash
    sudo systemctl daemon-reload  
    sudo systemctl enable minknow  
    sudo systemctl start minknow
```
8. Shutdown the computer/device and Power on the computer/device  

## How to just blast sequences?
If you wish to blast your new sequences to the existing database, the easiest is for you to start the docker container with `docker run -it --rm --gpus all -v $HOME:$HOME -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) thesanc/edna_minion_tools bash` and then run `blastn -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq" -max_target_seqs 5 -db path/to/reference/db -query path/to/fasta -out path/to/out/table`